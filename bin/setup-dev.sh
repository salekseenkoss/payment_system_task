#!/bin/sh

set -e

yarn install
bundle check || bundle install 
bundle exec rails db:setup
