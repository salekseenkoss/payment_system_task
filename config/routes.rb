# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :payments, only: [:create]
    end
  end

  get '/login' => 'user_sessions#new'
  post '/login' => 'user_sessions#create'
  delete '/logout' => 'user_sessions#destroy'

  resources :merchants
  resources :transactions, only: [:index]

  root 'merchants#index'
end
