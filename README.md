# README

## Application requirements

You need to have installed Docker (make sure that the `docker-compose` command is working).

## Development Setup

Clone repository:

```
git clone git@bitbucket.org:salekseenkoss/payment_system_task.git
```

Then run 

```
docker-compose up
docker-compose run app ./bin/setup-dev.sh
```

When docker compose is ready you can run tests:

```
docker-compose run -e RAILS_ENV=test app bundle exec rspec
```

## API endpoints

There is only one API endpoin to make payment.
You need to send HTTP request with `client_id` and `private_token` for the merchant.

```
merchant = Merchant.find(<id>)
app = merchant.applications.first
app.id              # => client_id
app.private_token   # => private_token

# Request
POST /api/v1/payments

# Request has url-form-encoded content type. You must provide next params:
#   - client_id
#   - customer_email
#   - amount  

# Headers:

Authorization: Bearer <private_token>

```
