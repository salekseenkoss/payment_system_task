require "rails_helper"

RSpec.feature "Merchants index page" do
  let!(:admin) { create(:admin) }

  before { sign_in_as_admin(admin) }

  describe 'Index page' do
    let!(:merchant1) { create(:merchant, name: 'Test name') }
    let!(:merchant2) { create(:merchant, :active) }
    let!(:transaction) { create(:authorize_transaction, merchant: merchant2) }

    scenario 'renders table with merchants information' do
      visit '/'

      expect(page).to have_text('Merchants')

      expect(page).to have_text('ID')
      expect(page).to have_text('Email')
      expect(page).to have_text('Name')
      expect(page).to have_text('Status')
      expect(page).to have_text('Sum of transactions')

      expect(page).to have_text(merchant1.email)
      expect(page).to have_text(merchant1.name)

      expect(page).to have_text(merchant2.email)
      expect(page).to have_text(merchant2.name)
    end

    scenario 'deletes merchant record' do
      visit '/'

      expect(page).to have_text('Merchants')

      row = page.find('tbody').find_all('tr').last
      expect(row).to have_text(merchant1.email)

      row.find('a', text: 'Delete').click

      expect(page).to_not have_text(merchant1.email)
      expect(page).to_not have_text(merchant1.name)
    end

    context 'when merchant has transactions' do
      scenario 'does not delete the merchant' do
        visit '/'

        expect(page).to have_text('Merchants')

        row = page.find('tbody').find_all('tr').first
        expect(row).to have_text(merchant2.email)

        row.find('a', text: 'Delete').click

        expect(page).to have_text(merchant2.email)
        expect(page).to have_text(merchant2.name)
      end
    end
  end
end
