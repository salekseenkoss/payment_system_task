require "rails_helper"

RSpec.feature "Merchants index page" do
  let!(:admin) { create(:admin) }

  before { sign_in_as_admin(admin) }

  describe 'Edit form' do
    let!(:merchant1) { create(:merchant, name: 'Test name') }

    scenario 'updates form' do
      visit "/merchants/#{merchant1.id}/edit"

      expect(page).to have_text('Edit merchant form')
      page.find('#merchant_name').fill_in(with: 'Updated name')
      page.find('#merchant_password').fill_in(with: '1q2w3E')
      page.find('#merchant_password_confirmation').fill_in(with: '1q2w3E')
      page.find('input[type="submit"]').click

      merchant1.reload
      expect(merchant1.name).to eq('Updated name')
      expect(merchant1.authenticate('1q2w3E')).to eq(merchant1)
    end
  end
end
