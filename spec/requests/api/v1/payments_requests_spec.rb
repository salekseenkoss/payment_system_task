require 'rails_helper'

RSpec.describe 'Payments requests', type: :request do
  describe 'POST /api/v1/payments' do
    let(:email) { Faker::Internet.email }
    let(:merchant) { create(:merchant, :active) }
    let(:client) { create(:merchant_application, merchant: merchant) }
    let(:params) { { amount: 130, customer_email: email } }

    context 'with valid params' do
      it 'returns serialized charge transaction' do
        post '/api/v1/payments', params: params.merge(auth_params(client)), headers: auth_header(client)

        expect(response).to have_http_status(200)
        expect(parsed_body.dig('data', 'id')).to be
        expect(parsed_body.dig('data', 'type')).to eq('charge')
        expect(parsed_body.dig('data', 'attributes', 'status')).to eq('approved')
        expect(parsed_body.dig('data', 'attributes', 'amount')).to eq('130.0')
        expect(parsed_body.dig('data', 'attributes', 'customer_email')).to eq(email)
      end
    end

    context 'when merchant is inactive' do
      let(:merchant) { create(:merchant, :inactive) }

      it 'returns 403 error' do
        post '/api/v1/payments', params: params.merge(auth_params(client)), headers: auth_header(client)

        expect(response).to have_http_status(403)
        expect(parsed_body).to eq(
          {
            'data' => nil,
            'errors' => { 'client' => ['Merchant account is inactive'] }
          }
        )
      end
    end

    context 'with wrong auth credentials' do
      it 'returns 401 error' do
        post '/api/v1/payments', params: params.merge(auth_params(client)), headers: {}

        expect(response).to have_http_status(401)
        expect(parsed_body).to eq(
          {
            'data' => nil,
            'errors' => { 'client' => ['Invalid authentication credentials'] }
          }
        )
      end
    end
  end
end
