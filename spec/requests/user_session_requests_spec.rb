require 'rails_helper'

RSpec.describe 'User session requests' do
  describe 'POST /login' do
    let!(:admin) { create(:admin) }

    let(:params) { { login: { email: admin.email, password: 'qwerty' } } }

    it 'creates new user session' do
      post '/login', params: params

      expect(response).to redirect_to(root_path)
      expect(flash[:notice]).to eq('Successfully logged in!')
      expect(session[:user_id]).to eq(admin.id.to_s)
    end

    context 'with incorrect params' do
      let(:params) { { login: { email: admin.email, password: 'wrong' } } }

      it 'renders login form with error message' do
        post '/login', params: params

        expect(response).to render_template(:new)
        expect(flash[:alert]).to eq("Incorrect email or password, try again.")
      end
    end
  end
end
