FactoryBot.define do
  factory :admin do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    description { 'Test description' }
    password { 'qwerty' }
    password_confirmation { 'qwerty' }

    trait :active do
      state { :active }
    end
  end
end
