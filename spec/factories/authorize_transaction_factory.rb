FactoryBot.define do
  factory :authorize_transaction do
    customer_email { Faker::Internet.email }
    amount { 100 }
    status { :approved }

    merchant
  end
end
