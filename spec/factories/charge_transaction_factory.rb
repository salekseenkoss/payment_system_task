FactoryBot.define do
  factory :charge_transaction do
    customer_email { Faker::Internet.email }
    amount { 100 }
    status { :approved }

    merchant
    authorize_transaction
  end
end
