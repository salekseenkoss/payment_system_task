FactoryBot.define do
  factory :reversal_transaction do
    customer_email { Faker::Internet.email }
    status { :approved }

    merchant
    authorize_transaction
  end
end
