FactoryBot.define do
  factory :merchant do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    description { 'Test description' }
    password { 'qwerty' }
    password_confirmation { 'qwerty' }

    trait :active do
      status { :active }
    end

    trait :inactive do
      status { :inactive }
    end
  end
end
