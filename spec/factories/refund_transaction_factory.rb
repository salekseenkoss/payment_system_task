FactoryBot.define do
  factory :refund_transaction do
    customer_email { Faker::Internet.email }
    amount { 100 }
    status { :approved }

    merchant
    charge_transaction
  end
end
