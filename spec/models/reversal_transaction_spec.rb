require 'rails_helper'

RSpec.describe ReversalTransaction do
  describe 'associations' do
    it { is_expected.to belong_to(:authorize_transaction) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:customer_email) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to allow_value(nil).for(:amount) }
  end

  describe 'callbacks' do
    let(:merchant) { create(:merchant, total_transaction_sum: 400) }

    context 'with authorize transaction' do
      let(:authorize_transaction) { create(:authorize_transaction, status: :approved, amount: 100, merchant: merchant) }

      let(:reversal_transaction) do
        build(:reversal_transaction, authorize_transaction: authorize_transaction, merchant: merchant)
      end

      it 'reverses AuthorizeTransaction automatically' do
        expect { reversal_transaction.save! }
          .to change { authorize_transaction.reload.status }.from('approved').to('reversed')
        expect(reversal_transaction.status).to eq('approved')
      end
    end
  end
end
