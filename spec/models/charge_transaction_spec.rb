require 'rails_helper'

RSpec.describe ChargeTransaction do
  describe 'associations' do
    it { is_expected.to belong_to(:authorize_transaction) }
    it { is_expected.to have_one(:refund_transaction) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:customer_email) }
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    it { is_expected.to validate_presence_of(:status) }
  end

  describe "after commit callbacks" do
    context 'with amount' do
      let(:merchant) { create(:merchant, total_transaction_sum: 400) }

      subject { build(:charge_transaction, amount: 200, merchant: merchant) }

      it "increases merchants total sums" do
        expect { subject.save! }.to change { merchant.reload.total_transaction_sum }.from(400).to(600)
      end
    end
  end
end
