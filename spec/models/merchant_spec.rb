require 'rails_helper'

RSpec.describe Merchant do
  describe 'associations' do
    it { is_expected.to have_many(:transactions) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to have_secure_password }
  end

  describe 'callbacks' do
    context 'after commit' do
      let(:merchant) { build(:merchant) }

      it 'creates new MerchantApplication relation' do
        merchant.save!
        expect(merchant.applications.count).to eq(1)
        expect(merchant.applications.first.private_token).to be
      end
    end
  end
end
