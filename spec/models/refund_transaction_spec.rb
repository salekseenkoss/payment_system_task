require 'rails_helper'

RSpec.describe RefundTransaction do
  describe 'associations' do
    it { is_expected.to belong_to(:charge_transaction) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:customer_email) }
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    it { is_expected.to validate_presence_of(:status) }
  end

  describe "after commit callbacks" do
    context 'with amount' do
      let(:merchant) { create(:merchant, total_transaction_sum: 400) }
      let!(:charge) { create(:charge_transaction, amount: 100, merchant: merchant) }

      subject { build(:refund_transaction, amount: 200, merchant: merchant) }

      it "decreases merchant's total sum" do
        expect { subject.save! }.to change { merchant.reload.total_transaction_sum }.from(500).to(300)
        expect(subject.charge_transaction.status).to eq('refunded')
      end
    end
  end
end
