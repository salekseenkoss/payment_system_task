require 'rails_helper'

RSpec.describe MerchantApplication, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:merchant) }
  end
end
