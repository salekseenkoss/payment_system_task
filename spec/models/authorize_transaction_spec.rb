require 'rails_helper'

RSpec.describe AuthorizeTransaction do
  describe 'associations' do
    it { is_expected.to have_one(:charge_transaction) }
    it { is_expected.to have_one(:reversal_transaction) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:customer_email) }
    it { is_expected.to validate_presence_of(:amount) }
    it { is_expected.to validate_numericality_of(:amount).is_greater_than(0) }
    it { is_expected.to validate_presence_of(:status) }
  end
end
