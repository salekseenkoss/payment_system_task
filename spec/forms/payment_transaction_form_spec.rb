require 'rails_helper'

RSpec.describe PaymentTransactionForm do
  describe '#save' do
    let(:params) { {} }
    let(:merchant) { create(:merchant) }
    let(:email) { Faker::Internet.email }
    subject(:form) { described_class.new(params) }

    context 'when params are valid' do
      let(:params) { { customer_email: email, merchant: merchant, amount: 300 } }

      it 'creates transactions' do
        expect(form.save).to eq(true)

        charge = form.charge_transaction
        expect(charge.merchant_id).to eq(merchant.id)
        expect(charge.amount).to eq(300)
        expect(charge.approved?).to be_truthy
        expect(charge.authorize_transaction.approved?).to be_truthy
      end
    end

    context 'when params are invalid' do
      let(:params) { { merchant: merchant, amount: -1 } }

      it 'returns validation errors' do
        expect(form.save).to eq(false)
        expect(form.errors.messages).to include(:customer_email, :amount)
      end
    end

    context 'when merchant is inactive' do
      let(:merchant) { create(:merchant, :inactive) }
      let(:params) { { customer_email: 'test2@example.com', merchant: merchant, amount: 300 } }

      it 'does not create transactions' do
        expect(form.save).to eq(false)
        expect(form.errors.messages[:merchant])
          .to eq([I18n.t('api.v1.payment_transaction_form.errors.merchant_inactive')])
      end
    end
  end
end
