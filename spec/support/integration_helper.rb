module IntegrationHelper
  def sign_in_as_admin(admin, password = 'qwerty')
    visit login_path
    fill_in 'login_email', with: admin.email
    fill_in 'login_password', with: password
    yield if block_given?
    click_button 'Log In'
    admin
  end
end
