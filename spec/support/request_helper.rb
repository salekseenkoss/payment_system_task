module RequestHelper
  def auth_header(client)
    { 'Authorization' => "Bearer #{client.private_token}" }
  end

  def auth_params(client)
    { client_id: client.id }
  end

  def parsed_body
    @parsed_body ||= JSON.parse(response.body)
  end
end
