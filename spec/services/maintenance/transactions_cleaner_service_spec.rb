require 'rails_helper'

RSpec.describe Maintenance::TransactionsCleanerService do
  describe '#call' do
    subject { described_class.new }

    let!(:tr1) { create(:authorize_transaction, created_at: 5.minutes.ago) }
    let!(:tr2) { create(:authorize_transaction, created_at: 2.hours.ago) }
    let!(:tr3) { create(:charge_transaction, created_at: 2.hours.ago, authorize_transaction: tr2) }

    context 'without argument' do
      it 'deletes all transaction records older than 1 hour' do
        expect { subject.call }.to change(Transaction, :count).from(3).to(1)
        expect(Transaction.first.id).to eq(tr1.id)
      end
    end

    context 'with argument' do
      let(:arg) { Time.zone.now }

      it 'deletes all transaction records order than argument value' do
        expect { subject.call(time: arg) }.to change(Transaction, :count).from(3).to(0)
      end
    end

    context 'when argument has incorrect type' do
      let(:arg) { 'foo' }

      it 'raises argument error' do
        expect { subject.call(time: arg) }
          .to raise_error(ArgumentError, 'Time must be instance of Time/DateTime/TimeWithZone')
      end
    end
  end
end
