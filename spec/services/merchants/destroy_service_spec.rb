require 'rails_helper'

RSpec.describe Merchants::DestroyService do
  subject { described_class.new }

  context 'when merchant has now transactions' do
    let!(:merchant) { create(:merchant) }

    it 'deletes the record' do
      expect { subject.call(merchant) }.to change(Merchant, :count).by(-1)
    end
  end

  context 'when merchant has transactions' do
    let(:merchant) { create(:merchant) }
    let!(:transaction) { create(:charge_transaction, merchant: merchant) }

    it 'does not delete the record' do
      expect { subject.call(merchant) }.to_not change(Merchant, :count)
    end
  end
end
