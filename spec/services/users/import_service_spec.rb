require 'rails_helper'

RSpec.describe Users::ImportService do
  describe '#call' do
    let(:file) { Rails.root.join('spec', 'fixtures', 'users_import.csv') }

    subject { described_class.new }

    it 'creates all users from CSV file with header' do
      expect { subject.call(file, headers: true) }.to change { User.count }.by(4)

      expect(Merchant.count).to eq(2)
      expect(Admin.count).to eq(2)
    end

    it 'inserts valid data for user' do
      result = subject.call(file, headers: true)

      expect(result.imported).to eq(4)
      expect(result.failed).to eq(0)
      expect(result.failed_emails).to eq([])

      admin = Admin.first
      expect(admin.email).to eq('sergei@example.com')
      expect(admin.name).to eq('Sergei Alekseenko')
      expect(admin.description).to eq('Test user')
      expect(admin.active?).to be_truthy
      expect(admin.authenticate('qwerty')).to eq(admin)
    end

    context 'with duplicates' do
      let(:file) { Rails.root.join('spec', 'fixtures', 'users_import_dups.csv') }
      let!(:merchant) { create(:merchant, email: 'merchant1@example.com') }

      it 'does not update if record exists' do
        result = nil
        expect { result = subject.call(file, headers: true) }.to_not change(Merchant, :count)

        old_attrs = merchant.attributes
        reloaded_attrs = merchant.reload.attributes
        expect(old_attrs).to eq(reloaded_attrs)

        expect(result.imported).to eq(0)
        expect(result.failed).to eq(2)
        expect(result.failed_emails).to eq(['merchant1@example.com', 'merchant1@example.com'])
      end
    end

    context 'with wrong file path' do
      let(:file) { Rails.root.join('spec', 'fixtures', 'wrong.csv') }

      it 'raises ArgumentError' do
        expect { subject.call(file, headers: true) }
          .to raise_error(ArgumentError, "CSV file #{file} does not exist")
      end
    end
  end
end
