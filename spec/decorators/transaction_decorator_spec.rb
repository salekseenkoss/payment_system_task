require 'rails_helper'

RSpec.describe TransactionDecorator do
  describe '.decorate' do
    let(:charge_transaction) { create(:charge_transaction, amount: 100) }

    it 'returns overrwiten transaction type' do
      presenter = described_class.decorate(charge_transaction)
      expect(presenter.type).to eq('charge')
      expect(presenter.id).to eq(charge_transaction.id)
    end
  end

  describe '.decorate_collection' do
    let!(:authorize) { create(:authorize_transaction, amount: 50) }
    let!(:charge) { create(:charge_transaction, amount: 50, authorize_transaction: authorize) }

    it 'decorates all records' do
      transactions = Transaction.where(id: [authorize.id, charge.id])
      decorated = described_class.decorate_collection(transactions)
      decorated.each { |item| expect(item.is_a?(described_class)).to be_truthy }
    end

    context 'with empty relation' do
      before { Transaction.delete_all }

      it 'returns empty array' do
        expect(described_class.decorate_collection(Transaction.all)).to eq([])
      end
    end
  end
end
