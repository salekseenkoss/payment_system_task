require 'securerandom'

namespace :transactions do
  task clean: :environment do
    jid = SecureRandom.hex(10)
    Rails.logger.info("[transactions:clean] Rake task ID=#{jid} started at #{Time.zone.now}")

    removed_rows_count = Maintenance::TransactionsCleanerService.new.call
    Rails.logger.info("[transactions:clean] Rake task ID=##{jid} finished. Rows=#{removed_rows_count}")
  end
end
