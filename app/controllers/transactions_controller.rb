# frozen_string_literal: true

class TransactionsController < ApplicationController
  before_action :authorize_admin!

  def index
    records = Transaction.all.order(id: :desc)
    @transactions = TransactionDecorator.decorate_collection(records)
  end
end
