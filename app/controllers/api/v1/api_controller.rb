module Api
  module V1
    class ApiController < ActionController::API
      include ActionController::HttpAuthentication::Token::ControllerMethods

      before_action :authenticate_client

      protected

      def request_http_token_authentication(_realm, _message)
        render json: { data: nil, errors: { client: ['Invalid authentication credentials'] } }, status: 401
      end

      def invalid_merchant_response
        render json: { data: nil, errors: { client: ['Merchant account is inactive'] } }, status: 403
      end

      private

      def current_app
        @current_app ||= MerchantApplication.find_by(id: params[:client_id])
      end

      def current_merchant
        @current_merchant ||= current_app&.merchant
      end

      def client_inactive?
        current_merchant.present? && current_merchant.inactive?
      end

      def authenticate_client
        authenticate_or_request_with_http_token do |token, _options|
          invalid_merchant_response if client_inactive?

          client_token = current_app&.private_token
          ActiveSupport::SecurityUtils.secure_compare(token, client_token.to_s)
        end
      end
    end
  end
end
