module Api
  module V1
    class PaymentsController < ApiController
      def create
        form = ::PaymentTransactionForm.new(payment_params.to_h.merge(merchant: current_merchant))

        if form.save
          render json: ChargeTransactionSerializer.new(form.charge_transaction)
        else
          render json: { errors: @form.errors.messages }
        end
      end

      private

      def payment_params
        params.permit(:amount, :customer_email, :customer_phone)
      end
    end
  end
end
