class UserSessionsController < ApplicationController
  def new; end

  # rubocop:disable Metrics/AbcSize
  def create
    user = User.find_by(email: login_params[:email].downcase)

    if user&.authenticate(login_params[:password])
      session[:user_id] = user.id.to_s
      redirect_to root_path, notice: 'Successfully logged in!'
    else
      flash.now.alert = "Incorrect email or password, try again."
      render :new
    end
  end
  # rubocop:enable Metrics/AbcSize

  def destroy
    session.delete(:user_id)
    redirect_to login_path, notice: "Logged out!"
  end

  private

  def login_params
    params.require(:login).permit(:email, :password)
  end
end
