# frozen_string_literal: true

class MerchantsController < ApplicationController
  before_action :authorize_admin!
  before_action :load_merchant, only: %i[destroy edit update]

  def index
    @merchants = Merchant.order(id: :desc)
  end

  def edit; end

  def update
    if @merchant.update(update_params)
      redirect_to merchants_path, notice: 'Merchant has been updated'
    else
      render :edit
    end
  end

  def destroy
    if Merchants::DestroyService.new.call(@merchant)
      redirect_to merchants_path, notice: 'Merchant has been deleted'
    else
      redirect_to merchants_path, alert: 'Unable to delete merchant with transactions'
    end
  end

  private

  def update_params
    params.require(:merchant).permit(:name, :description, :password, :password_confirmation)
  end

  def load_merchant
    @merchant = Merchant.find(params[:id])
  end
end
