# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  helper_method :current_user

  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  def authorize_admin!
    return if current_user.is_a?(Admin)

    redirect_to login_path, alert: 'You do not have permissions to visit that page'
  end
end
