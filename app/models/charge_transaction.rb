class ChargeTransaction < Transaction
  include WithRefTransactions

  has_one :refund_transaction, class_name: 'RefundTransaction', foreign_key: :ref_id
  belongs_to :authorize_transaction, class_name: 'AuthorizeTransaction', foreign_key: :ref_id

  validates :amount, presence: true, numericality: { greater_than: 0 }

  after_commit :increase_merchant_sum, on: :create

  private

  def increase_merchant_sum
    merchant.with_lock do
      current = merchant.total_transaction_sum || 0
      merchant.total_transaction_sum = current + amount
      merchant.save!
    end
  end
end
