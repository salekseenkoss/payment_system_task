class Merchant < User
  has_many :transactions
  has_many :applications, class_name: 'MerchantApplication', foreign_key: :merchant_id

  after_commit :create_application, on: :create

  private

  def create_application
    applications.create
  end
end
