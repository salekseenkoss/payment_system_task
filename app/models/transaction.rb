class Transaction < ApplicationRecord
  enum status: %i[approved reversed refunded error]

  belongs_to :merchant

  validates :customer_email, :merchant, :status, presence: true

  scope :older_than, ->(time) { where("created_at < ?", time) }
end
