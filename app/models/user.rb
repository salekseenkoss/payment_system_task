class User < ApplicationRecord
  enum status: %i[active inactive]
  has_secure_password

  validates :email, presence: true, uniqueness: true
  validates :status, presence: true
end
