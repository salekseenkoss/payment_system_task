class MerchantApplication < ApplicationRecord
  belongs_to :merchant

  before_create :generate_private_token

  private

  def generate_private_token
    self.private_token = SecureRandom.hex(16)
  end
end
