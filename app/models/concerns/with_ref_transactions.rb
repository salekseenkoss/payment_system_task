module WithRefTransactions
  extend ActiveSupport::Concern

  included do
    before_create :verify_referenced_transaction
  end

  class_methods do
    def referenced_transaction_association
      reflect_on_all_associations(:belongs_to)
        .detect { |assoc| assoc.foreign_key == :ref_id }&.name
    end
  end

  def ref
    @ref ||= begin
      name = self.class.referenced_transaction_association
      name.present? ? public_send(name.to_sym) : nil
    end
  end

  def verify_referenced_transaction
    return if ref.blank?
    return if ref.approved? || ref.refunded?

    self.status = :error
  end
end
