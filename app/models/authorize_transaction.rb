class AuthorizeTransaction < Transaction
  has_one :charge_transaction, class_name: 'ChargeTransaction', foreign_key: :ref_id
  has_one :reversal_transaction, class_name: 'ReversalTransaction', foreign_key: :ref_id

  validates :amount, presence: true, numericality: { greater_than: 0 }
end
