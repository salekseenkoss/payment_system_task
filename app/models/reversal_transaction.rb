class ReversalTransaction < Transaction
  belongs_to :authorize_transaction, class_name: 'AuthorizeTransaction', foreign_key: :ref_id

  after_create :reverse_authorize_transaction

  private

  def reverse_authorize_transaction
    authorize_transaction.reversed!
  end
end
