class RefundTransaction < Transaction
  belongs_to :charge_transaction, class_name: 'ChargeTransaction', foreign_key: :ref_id

  validates :amount, presence: true, numericality: { greater_than: 0 }

  before_create :refund_charge_transaction
  after_commit :decrease_merchant_sum, only: [:create]

  private

  def decrease_merchant_sum
    merchant.with_lock do
      current = merchant.total_transaction_sum || 0
      merchant.total_transaction_sum = current - amount
      merchant.save!
    end
  end

  def refund_charge_transaction
    charge_transaction.refunded!
  end
end
