class PaymentTransactionForm
  include ActiveModel::Model
  include ActiveModel::Validations
  # include ActiveModel::Serializers::JSON

  attr_accessor :merchant, :amount, :customer_email, :customer_phone,
                :authorize_transaction, :charge_transaction

  validates :merchant, presence: true
  validates :amount, presence: true, numericality: { greater_than: 0 }
  validates :customer_email, email: true, presence: true

  validate :merchant_state

  def save
    save!
  rescue StandardError
    false
  end

  def save!
    return persist! && true if valid?

    false
  end

  private

  def transaction_attrs
    {
      amount: amount,
      merchant: merchant,
      customer_email: customer_email,
      customer_phone: customer_phone
    }
  end

  def persist!
    ChargeTransaction.transaction { charge! }
  end

  def authorize!
    self.authorize_transaction = AuthorizeTransaction.create! transaction_attrs
  end

  def charge!
    authorize!

    self.charge_transaction = ChargeTransaction.create!(
      transaction_attrs.merge(authorize_transaction: authorize_transaction)
    )
  end

  def merchant_state
    return if merchant.active?

    errors.add(:merchant, I18n.t('api.v1.payment_transaction_form.errors.merchant_inactive'))
  end
end
