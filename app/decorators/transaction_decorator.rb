class TransactionDecorator < BaseDecorator
  def type
    object.type&.downcase&.split('transaction')&.first
  end
end
