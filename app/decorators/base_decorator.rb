class BaseDecorator < SimpleDelegator
  def self.decorate(input)
    if input.is_a?(Array)
      decorate_collection(input)
    elsif input.is_a?(ActiveRecord::Relation)
      decorate_collection(input)
    else
      decorate_object(input)
    end
  end

  def self.decorate_collection(objects)
    objects.map { |obj| decorate_object(obj) }
  end

  def self.decorate_object(obj)
    new(obj)
  end

  attr_accessor :object

  def initialize(input)
    self.object = input
    __setobj__(object)
  end

  private

  def h
    ApplicationController.helpers
  end
end
