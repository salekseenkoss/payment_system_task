module Users
  class BulkInsertService
    DEFAULT_BATCH_SIZE = 100

    attr_reader :inserted_emails, :queries_count

    def initialize
      @inserted_emails = []
      @queries_count = 0
    end

    def call(records, options: {})
      raise ArgumentError, 'records must be an instance of Array' unless records.is_a?(Array)

      batch_size = options.delete(:batch_size) || DEFAULT_BATCH_SIZE
      perform_insert(records, batch_size)
    end

    def inserted
      inserted_emails.count
    end

    private

    def perform_insert(records, batch_size)
      records.each_slice(batch_size) do |range|
        query = prepare_sql(prepare_sql_values(range))

        begin
          result = ActiveRecord::Base.connection.execute(query)
          @inserted_emails += result.map { |r| r['email'] }
        rescue StandardError
          next
        ensure
          @queries_count += 1
        end
      end
    end

    # The timestamps can be the same for all records
    # so we can memoise the current time
    def timestamp
      @timestamp ||= Time.zone.now
    end

    def prepare_sql_values(records)
      records.map do |record|
        "( #{record_to_array(record).join(',')} )"
      end
    end

    def record_to_array(record)
      [
        quote(record[:type]),
        quote(record[:email]),
        quote(record[:name]),
        quote(record[:description]),
        record[:status],
        quote(record[:password_digest]),
        quote(timestamp),
        quote(timestamp)
      ]
    end

    def prepare_sql(values)
      <<~SQL
        INSERT INTO users 
        (type,email,name,description,status,password_digest,created_at,updated_at) 
        VALUES #{values.join(', ')}
        ON CONFLICT DO NOTHING
        RETURNING email
      SQL
    end

    def quote(value)
      ActiveRecord::Base.connection.quote(value)
    end
  end
end
