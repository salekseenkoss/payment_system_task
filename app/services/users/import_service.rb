require 'csv'

module Users
  class ImportService
    CSV_HEADER = %i[type email name description status password].freeze
    ALLOWED_TYPES = %w[admin merchant].freeze

    attr_reader :imported, :failed, :failed_emails

    def initialize
      @users = []
      @imported = 0
      @failed = 0
      @failed_emails = []
    end

    # The service imports Merchants and Admins to the database
    # according the type field
    # The type field MUST be `admin` or `merchant`
    #
    # Valid structure of columns:
    #
    # type, email, name, description, status, password
    #
    def call(file_path, headers: false)
      raise ArgumentError, "CSV file #{file_path} does not exist" unless File.exist?(file_path)

      parse_file(file_path, headers: headers)
      import_users
    end

    private

    def parse_file(file_path, **options)
      ::CSV.foreach(file_path, **options) do |row|
        add_record(parse_row(row))
      end
    end

    def parse_row(row)
      out = {}
      CSV_HEADER.each_with_index { |col, index| out[col] = row[index] }
      out
    end

    def import_users
      importer = BulkInsertService.new
      importer.call(@users)
      @imported = importer.inserted
      @failed_emails = user_emails - importer.inserted_emails
      @failed = @failed_emails.count

      self
    end

    def add_record(record_hash)
      unless valid_record?(record_hash)
        @failed_emails << record_hash[:email]
        return
      end

      record_hash[:type] = record_hash[:type].classify
      record_hash[:password_digest] = password_digest(record_hash[:password])
      record_hash[:status] = User.statuses[record_hash[:status]] || 0
      @users << record_hash
    end

    def password_digest(password)
      BCrypt::Password.create(password)
    end

    # We don't call User's model validation to avoid calling
    # validates_uniqueness_of executes SQL query for each email
    #
    def valid_record?(record_hash)
      ALLOWED_TYPES.include?(record_hash[:type]) &&
        record_hash[:password].present? &&
        record_hash[:email].present?
    end

    def user_emails
      @users.map { |user| user[:email] }
    end
  end
end
