module Maintenance
  class TransactionsCleanerService
    NONE_AFFECTED = 0

    def call(time: nil)
      unless arg_valid?(time)
        raise(
          ArgumentError,
          'Time must be instance of Time/DateTime/TimeWithZone'
        )
      end

      time ||= default_time
      Transaction.older_than(time.in_time_zone).delete_all
    rescue ActiveRecord::ActiveRecordError => e
      Rails.logger.error(e)
      NONE_AFFECTED
    end

    private

    def default_time
      Time.zone.now - 1.hour
    end

    def arg_valid?(arg)
      return true if arg.blank?

      [ActiveSupport::TimeWithZone, DateTime, Time].include?(arg.class)
    end
  end
end
