module Merchants
  class DestroyService
    def call(merchant)
      return unless destroyable?(merchant)

      merchant.destroy
    end

    private

    def destroyable?(merchant)
      !merchant.transactions.exists?
    end
  end
end
