module Api
  module V1
    class ChargeTransactionSerializer
      include FastJsonapi::ObjectSerializer

      set_type :charge
      attributes :amount, :customer_email, :customer_phone, :status
    end
  end
end
