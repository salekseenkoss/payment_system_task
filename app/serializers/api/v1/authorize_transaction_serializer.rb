module Api
  module V1
    class AuthorizeTransactionSerializer
      include FastJsonapi::ObjectSerializer

      attributes :amount, :customer_email, :customer_phone, :status
    end
  end
end
