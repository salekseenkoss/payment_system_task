class CreateMerchantApplications < ActiveRecord::Migration[6.0]
  def change
    create_table :merchant_applications, id: :uuid do |t|
      t.string :private_token
      t.belongs_to :merchant
      t.timestamps
    end
  end
end
