class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.text :description
      t.string :email, null: false
      t.integer :status, default: 0
      t.integer :role, default: 0
      t.string :password_digest
      t.decimal :total_transaction_sum, default: 0, null: false
      t.string :type
      t.timestamps
    end
  end
end
