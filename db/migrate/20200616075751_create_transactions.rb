class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions, id: :uuid do |t|
      t.decimal :amount
      t.integer :status, default: 0
      t.string :customer_email
      t.string :customer_phone
      t.belongs_to :merchant
      t.integer :ref_id
      t.string :type
      t.timestamps
    end

    add_index :transactions, :ref_id
    add_index :transactions, :customer_email
  end
end
