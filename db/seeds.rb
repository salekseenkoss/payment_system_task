unless Admin.exists?
  Admin.create!({
                  email: 'admin@example.com',
                  name: 'Admin',
                  description: 'Website administrator',
                  password: 'qwerty',
                  password_confirmation: 'qwerty',
                  status: 'active'
                })
end

unless Merchant.exists?
  Merchant.create!({
                     email: 'merchant@example.com',
                     name: 'John Doe',
                     description: 'Merchant user',
                     password: 'qwerty',
                     password_confirmation: 'qwerty',
                     status: 'active'
                   })
end
